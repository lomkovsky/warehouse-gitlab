#!/bin/bash

echo "start deploy.sh"
set -e
ssh -i "key.pem" ubuntu@$DEPLOY_SERVERS 'bash' < ./deploy/updateAndRestart.sh
