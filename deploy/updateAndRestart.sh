#!/bin/bash

echo "start updateAndRestart.sh"
set -e
# Delete the old repo
rm -rf my_app/warehouse-gitlab
cd my_app
git clone https://gitlab.com/lomkovsky/warehouse-gitlab.git
mkdir ./warehouse-gitlab/src/config
cp docker.env ./warehouse-gitlab/src/config/docker.env
cd warehouse-gitlab
# Restart docker
sudo docker stop $(sudo docker ps -a -q)
sudo docker rm $(sudo docker ps -a -q)
sudo docker rmi $(sudo docker images -q)
sudo docker-compose up --build -d


echo "Server start!!!"
