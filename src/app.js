/* eslint-disable consistent-return */
/* eslint-disable no-console */
const express = require('express');

const app = express();
const passport = require('passport');
const Sentry = require('@sentry/node');
const productsRouter = require('./routers/products');
const categoriesRouter = require('./routers/categories');
const userRouter = require('./routers/user');
const worker = require('./RabbitMQ/consumer');

Sentry.init({
  dsn: 'https://ede314c854f04925a520710468408c48@lomkovsky.duckdns.org/2',
  maxBreadcrumbs: 50,
  debug: true,
});

// returns JSON
app.use(express.json());
// passport config for test ci
require('./middleware/passport')(passport);

// passport middleware
app.use(passport.initialize());

// set static view engine as pug
app.set('view engine', 'pug');
app.get('/', (req, res) => {
  res.render('index', {
    title: 'Home',
  });
});

// connection routers of /products
app.use(productsRouter);
// connection routers of /categories
app.use(categoriesRouter);
// connection routers of /user
app.use(userRouter);

app.use((err, req, res, next) => {
  if (err) {
    Sentry.captureException(err);
    return res.status(err.number || 503).json({ error: err.message });
  }
  next();
});

// Сatch the unhandled Rejection
process.on('unhandledRejection', (err) => {
  console.log('Caught exception: ', err);
  Sentry.captureException(err);
});
process.on('uncaughtException', (err) => {
  console.log('Caught uncaughtException: ', err);
  Sentry.captureException(err);
});

// waiting for a request for delete products from removed a category
worker();

// inform the Sentry Node SDK about your DSN
// Sentry.init({ dsn: 'https://05061ddc7e3b4b3c9a4c79cdca2fc421@sentry.io/1786074' });
// myUndefinedFunction();
module.exports = app;
