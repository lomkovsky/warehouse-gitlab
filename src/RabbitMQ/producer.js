/* eslint-disable no-console */

const amqp = require('amqplib');

let url;
if (process.env.MONGODB_URL === 'mongodb://mongo:27017/warehouse' || process.env.MONGODB_URL === 'mongodb://mongo:27017/warehouse-test') {
  url = 'amqp://rabbitmq:5672';
} else if (process.env.MONGODB_URL === 'mongodb://mongo/warehouse' || process.env.MONGODB_URL === 'mongodb://mongo/warehouse-test') {
  url = 'amqp://rabbitmq:5672';
} else {
  url = 'amqp://localhost';
}
const connection = async (queueName) => {
  const conn = await amqp.connect(url);
  const channel = await conn.createChannel();
  await channel.assertQueue(queueName, { durable: true });
  return channel;
};

const sendToQueue = async (queueName, categoryId) => {
  const channel = await connection(queueName);
  const msg = categoryId.toString();
  channel.sendToQueue(queueName, Buffer.from(msg), {
    persistent: true,
  });
  console.log('[x] SentId ', msg);
};

module.exports = sendToQueue;
