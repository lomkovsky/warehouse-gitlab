/* eslint-disable no-unused-vars */
/* eslint-disable consistent-return */
const express = require('express');
const passport = require('passport');
const asyncHandler = require('express-async-handler');

const router = new express.Router();
const Category = require('../models/category');


// read all categories
// router.get('/categories', async (req, res) => {
router.get('/categories', asyncHandler(async (req, res, next) => {
  try {
    const categories = await Category.find().populate('products');
    res.send(categories);
  } catch (e) {
    const err = new Error('Something went wrong');
    err.number = 500;
    throw err;
  }
}));

// create a new categories
router.post('/categories', passport.authenticate('jwt', { session: false }), asyncHandler(async (req, res) => {
  const category = new Category(req.body);
  try {
    await category.save();
    res.status(201).send(category);
  } catch (e) {
    const err = new Error(e.message);
    err.number = 400;
    throw err;
  }
}));

// delete category by id
router.delete('/categories/:id', passport.authenticate('jwt', { session: false }), asyncHandler(async (req, res) => {
  try {
    const category = await Category.findById(req.params.id);
    if (!category) {
      const err = new Error('category not found');
      err.number = 404;
      throw err;
    }
    await category.remove();
    res.status(204).send();
  } catch (e) {
    const err = new Error(e.message);
    err.number = 404;
    throw err;
  }
}));

// update category
router.patch('/categories/:id', passport.authenticate('jwt', { session: false }), asyncHandler(async (req, res) => {
  try {
    const category = await Category.findByIdAndUpdate(req.params.id,
      req.body,
      { new: true });
    if (!category) {
      const err = new Error('category not found');
      err.number = 404;
      throw err;
    }
    res.send(category);
  } catch (e) {
    const err = new Error(e.message);
    err.number = 404;
    throw err;
  }
}));

// read category
router.get('/categories/:id', asyncHandler(async (req, res) => {
  try {
    const category = await Category.findById(req.params.id);
    if (!category) {
      const err = new Error('category not found');
      err.number = 404;
      throw err;
    }
    res.send(category);
  } catch (e) {
    const err = new Error(e.message);
    err.number = 404;
    throw err;
  }
}));
module.exports = router;
