/* eslint-disable consistent-return */
const express = require('express');
const asyncHandler = require('express-async-handler');

const router = new express.Router();
const passport = require('passport');
const Category = require('../models/category');
const Product = require('../models/product');

// create a new product
router.post('/products', passport.authenticate('jwt', { session: false }), asyncHandler(async (req, res) => {
  const category = await Category.findById(req.body.category);
  if (!category) {
    const err = new Error('product not found!');
    err.number = 404;
    throw err;
  } else {
    try {
      let product = new Product(req.body);
      await product.save();
      product = await product.populate('category').execPopulate();
      res.send(product);
    } catch (e) {
      const err = new Error(e.message);
      err.number = 400;
      throw err;
    }
  }
}));

// read all products from all categories
router.get('/products', asyncHandler(async (req, res) => {
  try {
    if (req.query.category) {
      const products = await Product.find({ category: req.query.category }).populate('category');
      res.send(products);
    } else {
      const products = await Product.find().populate('category');
      res.send(products);
    }
  } catch (e) {
    const err = new Error(e.message);
    err.number = 400;
    throw err;
  }
}));

// delete product
router.delete('/products/:id', passport.authenticate('jwt', { session: false }), asyncHandler(async (req, res) => {
  try {
    const product = await Product.findByIdAndDelete(req.params.id);
    if (!product) {
      const err = new Error('product not found!');
      err.number = 404;
      throw err;
    }
    res.status(204).send();
  } catch (e) {
    const err = new Error(e.message);
    err.number = 404;
    throw err;
  }
}));

// update product
router.patch('/products/:id', passport.authenticate('jwt', { session: false }), asyncHandler(async (req, res) => {
  try {
    let product = await Product
      .findOneAndUpdate({ _id: req.params.id },
        req.body,
        { new: true });
    if (!product) {
      const err = new Error('product not found!');
      err.number = 404;
      throw err;
    }
    product = await product.populate('category').execPopulate();
    res.send(product);
  } catch (e) {
    const err = new Error(e.message);
    err.number = 404;
    throw err;
  }
}));

// read product
router.get('/products/:id', asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id).populate('category');
  if (!product) {
    const err = new Error('product not found!');
    err.number = 404;
    throw err;
  }
  res.send(product);
}));

module.exports = router;
