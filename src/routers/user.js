/* eslint-disable no-unused-vars */
/* eslint-disable consistent-return */
const express = require('express');
const asyncHandler = require('express-async-handler');

const router = new express.Router();
const passport = require('passport');
const User = require('../models/user');

// register page
router.post('/users', asyncHandler(async (req, res) => {
  const { name, email, password } = req.body;
  // check required fields
  if (!name || !email || !password) {
    const err = new Error('Required fields missing');
    err.number = 400;
    throw err;
  }
  // check password length
  if (password.length < 6) {
    const err = new Error('Password less than 6 character');
    err.number = 400;
    throw err;
  }
  try {
    // create a new user
    let user = new User(req.body);
    await user.save();
    const token = await user.generateAuthToken();
    user = await user.publicFields();
    res.status(201).send({ user, token });
  } catch (e) {
    const err = new Error(e.message);
    err.number = 400;
    throw err;
  }
}));

// error logging handle
router.get('/users/login/error', asyncHandler(async (req, res) => {
  const err = new Error('error logging');
  err.number = 401;
  throw err;
}));

// login handle
router.post('/users/login',
  passport.authenticate('local', {
    failureRedirect: '/users/login/error',
  }),
  asyncHandler(async (req, res) => {
    const { user } = req;
    const token = await user.generateAuthToken();
    const welcomeMassage = `Welcome ${user.name}!!`;
    res.send({ welcomeMassage, token });
  }));

// get my profile
router.get('/users/me', passport.authenticate('jwt', { session: false }), asyncHandler(async (req, res) => {
  try {
    const publicUser = await req.user.publicFields();
    res.send(publicUser);
  } catch (e) {
    const err = new Error(e.message);
    err.number = 500;
    throw err;
  }
}));

// update my profile
router.patch('/users/me', passport.authenticate('jwt', { session: false }), asyncHandler(async (req, res) => {
  const updates = Object.keys(req.body);
  const allowedUpdates = ['name', 'email', 'password'];
  const isValidOperation = updates.every((update) => allowedUpdates.includes(update));
  if (!isValidOperation) {
    const err = new Error('Invalid filds for updates!');
    err.number = 400;
    throw err;
  }
  if (req.body.password && req.body.password.length < 6) {
    const err = new Error('Password less than 6 character');
    err.number = 400;
    throw err;
  }
  try {
    // eslint-disable-next-line no-underscore-dangle
    let user = await User.findById(req.user._id);
    if (!user) {
      const err = new Error('user not found');
      err.number = 404;
      throw err;
    }
    Object.assign(user, req.body);
    await user.save();
    user = await user.publicFields();
    res.send(user);
  } catch (e) {
    const err = new Error(e.message);
    err.number = 404;
    throw err;
  }
}));

// delete my profile
router.delete('/users/me', passport.authenticate('jwt', { session: false }), asyncHandler(async (req, res) => {
  try {
    await req.user.remove();
    res.status(204).send();
  } catch (e) {
    const err = new Error(e.message);
    err.number = 500;
    throw err;
  }
}));

module.exports = router;
