/* eslint-disable no-console */
const mongoose = require('mongoose');

const connectionURL = process.env.MONGODB_URL;
// const connectionURL = 'mongodb://mongo:27017/warehouse-docker';
console.log('connectionURL ', connectionURL);
mongoose.connect(connectionURL, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
});
